import unittest
from pom.home_page import HomePage
from pom.login_page import LoginPage
from pom.create_topic_page import CreateTopicPage
from pom.topic_detail_page import TopicDetailPage
from testcases.basecase import MyBaseCase

class TestTopics(MyBaseCase):

    def test_create_topic(self):
        self.cp = CreateTopicPage()
        self.tpdetail = TopicDetailPage()
        self.hp.click_create_topic_btn()
        self.cp.create_topic_with_contents('helloworld','分享',"7777999999999")

        # 添加断言
        self.assertEqual( self.tpdetail.get_title_text(), 'helloworld')
        self.assertEqual(self.tpdetail.get_tab_text(),'分享')
        self.assertEqual(self.tpdetail.get_content_text(),'7777999999999')



if __name__ == '__main__':
    unittest.main()