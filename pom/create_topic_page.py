from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains

from pom.base_page import BasePage
from pom.home_page import HomePage
from pom.login_page import LoginPage


class CreateTopicPage(BasePage):

    def create_topic_with_contents(self, title, tab, content):
        title_input = self.driver.find_element(By.XPATH, '//*[@id="title"]')
        title_input.clear()
        title_input.send_keys(title)

        tab_select = self.driver.find_element(By.ID, "tab-value")
        tab_select.click()
        options = self.driver.find_elements(By.XPATH, '//select[@name="tab"]/option')
        for op in options:
            if op.text == tab:
                op.click()
                break
        else:
            raise Exception(f'找不到{tab}元素')

        content_input = self.driver.find_element(By.XPATH, '//div[@class="CodeMirror-scroll"]')
        content_input.click()

        ac = ActionChains(self.driver)
        ac.move_to_element(content_input).send_keys(content).perform()

        self.driver.find_element(By.XPATH, '//*[@value="提交"]').click()


if __name__ == '__main__':
    hp = HomePage()
    hp.click_link_new_page_by_text('登录')
    lp = LoginPage()
    lp.user_login_with_username_password('fanmao1','123456')
    hp.click_create_topic_btn()
    cp = CreateTopicPage()
    cp.create_topic_with_contents('helloworld','分享','helloworld')