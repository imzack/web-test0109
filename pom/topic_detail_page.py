from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains

from pom.base_page import BasePage

class TopicDetailPage(BasePage):

    def get_title_text(self):
        return  self.driver.find_element(By.XPATH,'//span[@class="topic_full_title"]').text

    def get_tab_text(self):
        ele_text:str = self.driver.find_element(By.XPATH,'//div[@class="changes"]/span[last()]').text
        return ele_text.split(' ')[-1]

    def get_content_text(self):
        return self.driver.find_element(By.XPATH,'//div[@class="topic_content"]').text

    def reply_topic(self, reply_content):
        ele = self.driver.find_element(By.XPATH,'//div[@class="CodeMirror-scroll"]')
        ele.click()

        ac = ActionChains(self.driver)
        ac.move_to_element(ele).send_keys(reply_content).perform()

        self.driver.find_element(By.XPATH,'//input[@value="回复"]').click()