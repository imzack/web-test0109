

import unittest
from ddt import ddt,data,unpack
testdata = [(1, 2), (4, 5), (9, 0), (7, 6)]

@ddt
class TestA(unittest.TestCase):
    @unittest.skip
    @data((1,2),(4,5),(9,0),(7,6))
    @unpack
    def test_a(self,x,y):
        self.assertTrue(x > y)

    @data(*testdata)   # [(1, 2), (4, 5), (9, 0), (7, 6)] 拆开  (1,2),(4,5),(9,0),(7,6)
    @unpack
    def test_b(self, x, y):
        self.assertTrue(x > y)